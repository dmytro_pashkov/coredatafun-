//
//  ViewController.swift
//  CoreDataFun
//
//  Created by Dmytro Pashkov on 4/3/19.
//  Copyright © 2019 akvelon. All rights reserved.
//

import UIKit
import CoreData

class MainVC: UIViewController {
    
    //MARK: - outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - vars
    
    private let dataManager = CoreDataManager.shared
    
    // MARK: - actions
    
    @IBAction func addRecord(_ sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        dataManager.fetchedResultsController.delegate = self
    }
    
}



extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Can call – dataManager.fetchResultController.fetchedObjects?.count
        // transfer via reference or value ???
        
        guard let sections = dataManager.fetchedResultsController.sections else { return 1 }
        return sections.first?.numberOfObjects ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "device_cell")
        
        let dataObject = dataManager.fetchedResultsController.object(at: indexPath)
        cell.textLabel?.text = dataObject.name
        cell.detailTextLabel?.text = String(dataObject.price)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let object = dataManager.fetchedResultsController.object(at: indexPath)
            dataManager.fetchedResultsController.managedObjectContext.delete(object)
            dataManager.saveChanges()
        }
    }
}


extension MainVC: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
}
