//
//  AddRecordVC.swift
//  CoreDataFun
//
//  Created by Dmytro Pashkov on 4/5/19.
//  Copyright © 2019 akvelon. All rights reserved.
//

import UIKit

class AddRecordVC: UIViewController {

    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBAction func createRecord() {
        /*let device = Device(context: CoreDataManager.shared.fetchedResultsController.managedObjectContext)
        device.name = nameTextField.text ?? "nan"
        device.price = Float(priceTextField.text ?? "-1.0") ?? -1.0
        
        CoreDataManager.shared.saveChanges()
        */
        let name = nameTextField.text ?? "nan"
        let price = Float(priceTextField.text ?? "-1.0")
        
        CoreDataManager.shared.makeRecord(name: name, price: price ?? -1.0)
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
