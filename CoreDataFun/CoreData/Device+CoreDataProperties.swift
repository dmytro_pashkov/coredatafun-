//
//  Device+CoreDataProperties.swift
//  CoreDataFun
//
//  Created by Dmytro Pashkov on 4/4/19.
//  Copyright © 2019 akvelon. All rights reserved.
//
//

import Foundation
import CoreData


extension Device {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Device> {
        return NSFetchRequest<Device>(entityName: "Device")
    }

    @NSManaged public var name: String?
    @NSManaged public var price: Float
}
