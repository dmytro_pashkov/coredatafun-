//
//  CoreDataManager.swift
//  CoreDataFun
//
//  Created by Dmytro Pashkov on 4/3/19.
//  Copyright © 2019 akvelon. All rights reserved.
//

import Foundation
import CoreData


final class CoreDataManager {
    
    public static let shared = CoreDataManager()
    
    // MARK: - vars
    
    private(set) var fetchedResultsController: NSFetchedResultsController<Device>!
    
    private let persistentContainer = NSPersistentContainer(name: "CoreDataFun")
    
    private let managedObjectContext: NSManagedObjectContext
    
    private let entityDescription: NSEntityDescription
    
    // MARK: - init
    
    private init() {
        // initializing CoreData stack
        persistentContainer.loadPersistentStores() { _, error in
            guard error == nil else { fatalError() }
        }
        
        managedObjectContext = persistentContainer.viewContext
        entityDescription = NSEntityDescription.entity(forEntityName: "Device", in: managedObjectContext)!
        
        setupFetchedResultsController()
    }
    
    // MARK: - public funcs
    
    public func makeRecord(name: String, price: Float) {
        let device = Device(context: managedObjectContext)
        device.name = name
        device.price = price
        
        saveChanges()
    }
    
    public func saveChanges() {
        if !managedObjectContext.hasChanges { return }
        
        do {
            try managedObjectContext.save()
        } catch {
            print("CoreData Error: Fail to save context")
        }
    }

    public func getAllDevices() -> [Device] {
        return fetchManagedObjects(predicate: nil)
    }
    
    // MARK: - private funcs
    
    private func fetchManagedObjects(predicate: NSPredicate?) -> [Device] {
        let fetchRequest = NSFetchRequest<Device>(entityName: entityDescription.name!)
        fetchRequest.predicate = predicate
        
        do {
            let managedObjects = try managedObjectContext.fetch(fetchRequest)
            return managedObjects
        } catch { return [] }
    }
    
    // MARK: - CoreData support funcs
    
    private func setupFetchedResultsController() {
        let fetchRequest = NSFetchRequest<Device>(entityName: entityDescription.name!)
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.localizedStandardCompare(_:)))
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: managedObjectContext,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)
        do {
            try fetchedResultsController.performFetch()
        } catch { }
    }
}
